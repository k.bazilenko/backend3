
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Задание 3</title>
</head>

<body>
    <div class="main">
    <div class="block_4">
        <h1>Форма</h1>
        <form method="POST" id="form">
            <label>Имя:<br />
                <input type=text name="field-name" placeholder="Введите ваше имя" />
            </label><br />

            <label>E-mail:<br />
                <input name="field-email" placeholder="Введите ваш e-mail" type="email">
            </label><br />

            <label>Год рождения:<br />
            <select name="field-date">
                <option>1990</option>
                <option>1991</option>
                <option>1992</option>
                <option>1993</option>
                <option>1994</option>
                <option>1995</option>
                <option>1996</option>
                <option>1997</option>
                <option>1998</option>
                <option>1999</option>
                <option>2000</option>
                <option>2001</option>
                <option>2002</option>
                <option>2003</option>
                <option>2004</option>
                <option>2005</option>
                <option>2006</option>
                <option>2007</option>
                <option>2008</option>
                <option>2009</option>
                <option>2010</option>
                <option>2011</option>
                <option>2012</option>
                <option>2013</option>
                <option>2014</option>
                <option>2015</option>
                <option>2016</option>
                <option>2017</option>
                <option>2018</option>
                <option>2019</option>
                <option>2020</option>
                <option>2021</option>
            </select>
        </label><br />

            <label>Пол:</label><br />
            <label class="radio"><input type="radio" checked="checked" name="radio-sex" value=1 />Мужской
            </label>
            <label class="radio"><input type="radio" name="radio-sex" value=0 />Женский
            </label><br />

            <label>Выберите кол-во конечностей:</label><br />
            <label class="radio"><input type="radio" checked="checked" name="radio-limb" value=0 />0
            </label>
            <label class="radio"><input type="radio" name="radio-limb" value=1 />1
            </label>
            <label class="radio"><input type="radio" name="radio-limb" value=2 />2
            </label>
            <label class="radio"><input type="radio" name="radio-limb" value=3 />3
            </label>
            <label class="radio"><input type="radio" name="radio-limb" value=4 />4
            </label><br />

            <label>Ваши сверхспособности:<br />
                <select multiple="true" name="superpower[]">
                    <option value="Бессмертие">Бессмертие</option>
                    <option value="Прохождение сквозь стены">Прохождение сквозь стены</option>
                    <option value="Левитация">Левитация</option>
                </select>
            </label><br />

            <label>
                Биография:<br />
                <textarea name="BIO" placeholder="Расскажите о себе"></textarea>
                <br />
            </label>

            <label>
                <input name="ch" type="checkbox" checked=checked value=1>С контрактом ознакомлен:<br />
            </label>

            <input type="submit" value="Отправить" />
        </form>
    </div>
</div>
</div>
   
</body>

</html>

